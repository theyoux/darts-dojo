Coding Dojo : Dart Game
=======================

Ce dojo fonctionne par équipe de 2 à 4 personnes.

Il se compose de 2 sujets complémentaires, vous pouvez travailler sur l'un ou l'autre, ils sont indépendants.

Vous disposez d'environ 1h30 pour réfléchir, organiser et produire un code **en équipe** qui représente l'état de l'art de vos connaissances en programmation.

À l'issue du dojo, vous pourrez présenter aux autres codeurs votre approche et l'expérience que cela vous a apporté.

N'essayez de traiter le sujet restant que si **vous et votre équipe** êtes satisfait de votre prestation sur le premier.

## Le jeu de fléchettes

Il existe de nombreuses variantes du jeu de fléchette, celui que nous décrivons ici s'appelle le "501".

Un dessin valant mieux qu'une image :

![Darts](http://www.darts501.com/img/Dartboard_Score_Segments750.gif)


### Comptage des points

La cible retenue pour ce jeu est de type horloge, c'est à dire composée de 20 quartiers numérotés de 1 à 20.

Chaque quartier est traversé par 2 segments, le premier extérieur "compte double", le second au milieu des quartiers "compte triple".

Au centre de la cible, un cercle rouge désigne le "BULL" et vaut 50 points.

Autour du "BULL", un cercle vert vaut 25 points.

Le "BULL" est donc un "compte double" qui vaut 2 x 25.


### Règle du jeu
_Extrait du site http://dartsffd.free.fr/bases1.html_

_Il existe de nombreux jeux et chacun a de nombreuses variantes. La règle utilisée par 3 millions de compétiteurs à travers le monde est le 501: après chaque volée (lancer de 3 fléchettes) le total des points obtenus est décompté du capital initial de 501 du joueur ou de l'équipe._

_Gagne la partie celui qui arrive à zéro le premier, mais en terminant à zéro exactement et par un double._

_Si un joueur marque un total de points supérieur au capital restant, ce total ne compte pas et le joueur conserve ce capital._

_Exemple : pour finir la partie sur un score restant de 97 points, on peut faire "triple 19" puis "double 20". Si le joueur fait "triple 19", puis "triple 20" (soit 117 points), il lui restera 97 à la volée suivante._



## Sujet n°1 : résoudre les coups gagnants

Un joueur démarre avec un score de 501.

Au fil du jeu, son score diminue.

Pour remporter la partie, il doit avec une même volée (3 fléchette) effectuer 
le nombre de points restant **en terminant** par un "compte double".

Le coup gagnant ne nécessite pas forcément l'usage des 3 fléchettes.

Le but de cet exercice est de concevoir et de produire l'algorithme qui, pour un score donné, indique au joueur ses possibilités de faire un coup gagnant.

Exemple :

    Étant donné que Sébastien a un score de 81
    Quand Sébastien demande quels sont les coups gagnants
    Alors le résultat devrait être
        Triple 15, Double 18
        Triple 19, Double 12

L'algorithme se limite à restituer "n" coups gagnants les plus performants.


## Sujet n°2 : gestion des scores

Une partie de fléchette peut accueillir plusieurs joueurs.

Chaque joueur est identifié par son prénom et son tour de passage.

À chaque passage, le joueur indique la valeur totale de sa volée (les 3 fléchettes).

Un historique présente au fil du jeu les scores détaillés de de chacuns des joueurs.

En cas d'erreur lors d'une saisie, il faut pouvoir revenir en arrière dans l'historique de jeu, et donc annuler un certain nombre de saisies.

Dans le cas ou un joueur arrive exactement à zéro avec un "compte double", le jeu s'arrête et le joueur est donné vainqueur.

Dans le cas ou un joueur arrive au terme de sa volée sur un score de 1, le joueur est disqualifié et le jeu continue avec les autres joueurs.

Dans le cas ou un joueur dépasse la valeur lui permettant d'arriver à zéro, ou qu'il arrive à zéro sans terminer par un "compte double", sa dernière volée n'est pas conservée, son score reste intact et le tour passe à un autre joueur.


